
	<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
	    <?php if ($picture) {
	      print $picture;
	    }?>	
	    <?php if ($page == 0) { ?><h2><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php }; ?>

	    <div class="taxonomy"><?php print $terms?></div>

		<div class="nodebody">
			<?php print $content?>
		</div>

		<span class="submitted"><?php print $submitted?></span>
		<?php if ($links) { ?><div class="links">&raquo; <?php print $links?></div><?php }; ?>
	</div>
