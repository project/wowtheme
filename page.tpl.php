<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <?php print $head ?>
  <title><?php print $head_title ?></title>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>

<div id="wrapper"><div id="everything">

    <div id="header">
		<?php if ($logo) { ?><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
		<?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a><?php if ($site_slogan) { ?><?php print ' - ' .$site_slogan ?><?php } ?></h1><?php } ?>
		<?php print $header ?>
    </div>

    <?php if (isset($secondary_links) or isset($primary_links) or $search_box) { ?>
		<div id="navigation">
			<?php if (isset($secondary_links)) { ?><?php print theme('links', $secondary_links, array('class' => 'links', 'id' => 'subnavlist')) ?><?php } ?>
			<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'navlist')) ?><?php } ?>
			<div id="search-box"><?php print $search_box ?></div>
		</div>
	<?php } ?>

	<div id="middle">

		<div id="breadcrumbs"><?php print $breadcrumb ?></div>

		<?php if ($left) { ?>
			<div id="left_column">
	      		<?php print $left ?>
			</div>
		<?php } ?>
	
        <div id="middle_column" <?php if (!$left or !$right) { echo 'class="two_column"'; } ?>>

		<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
			<div id="main">
				<h1 class="title"><?php print $title ?></h1>
				<div class="tabs"><?php print $tabs ?></div>
				<?php if ($show_messages) { print $messages; } ?>
				<?php print $help ?>
				<?php print $content; ?>
				<?php print $feed_icons; ?>
			</div>
        </div>

       	<?php if ($right) { ?>
	        <div id="right_column">
		      <?php print $right ?>
	        </div>
	    <?php } ?>

        <br clear="all">
    </div>
	
	<div id="footer">
	  <?php print $footer_message ?>
	  <?php print $footer ?>
	  This theme made possible by Hayden Hawke's <a href="http://www.secretgoldguide.com">Secret Gold Guide</a><br>
	  <span class="copyright">World of Warcraft&reg; and Blizzard Entertainment&reg; are all trademarks or registered trademarks of Blizzard Entertainment in the United States and/or other countries. <br> These terms and all related materials, logos, and images are copyright &copy; Blizzard Entertainment. This site is in no way associated with or endorsed by Blizzard Entertainment&reg;.</span></div>
	</div>

</div></div>

<?php print $closure ?>

</body>
</html>
